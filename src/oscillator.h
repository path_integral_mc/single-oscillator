//  Path Integral Monte Carlo for a single oscillator
//  Copyright (C) 2020 Purrello V.
//  This program is in public domain. It can be modified and relicensed.

#ifndef OSCILLATOR_H_14542
#define OSCILLATOR_H_14542

#include <vector>

class Oscillator
{
    public:
        Oscillator (int _M=8, bool _VERBOSE=true);
        void print_x();
        void set_x0(double x0);

    protected:
        double calc_E(double beta);
        double calc_E_exact(double beta);
        double calc_H2(double beta);
        double calc_H2_exact(double beta);
        double rho_free(double x1, double x2, double beta);
	double partial2_rho_free(double x1, double x2, double beta);
        double rho(double x1, double x2, double beta);
        double V(double x);
        double Z(double beta);
        double rho_ho(double x1, double x2, double beta);

	const int M; //number of time slices
        const bool VERBOSE;
        std::vector<double> x; //displacements from equilibrium of M "atoms"
};
#endif /* OSCILLATOR_H_14542 */
