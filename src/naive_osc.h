//  Path Integral Monte Carlo for a single oscillator
//  Copyright (C) 2020 Purrello V.
//  This program is in public domain. It can be modified and relicensed.

#ifndef NAIVE_OSCILLATOR_H_32504
#define NAIVE_OSCILLATOR_H_32504

#include "oscillator.h"
#include <vector>
#include <random>

class Naive_osc: public Oscillator
{
    public:
        Naive_osc (double _beta, int _M=8, long _MCsteps=1e6, double _delta=1, bool _VERBOSE=true);
        void thermalize();
        void run_steps();
        double get_Evir();
        double get_E();
        double get_E_sigma2();
        double get_E2();
        double get_E2_sigma2();
        double get_Cv();

    protected:
        bool Metropolis_step_accepted();

        std::random_device rd;  //Will be used to obtain a seed for the random number engine
        std::mt19937 gen; //Standard mersenne_twister_engine seeded with rd()
        std::uniform_real_distribution<> rng_real_01;
        std::uniform_int_distribution<> rng_int_0M;
        const double beta;
        const long MCsteps; //number of Monte Carlo steps in simulation
        const double delta; //Metropolis step size in x
        const double delta_tau; //imaginary time step
        double Evir, E, E_sigma2, E2, E2_sigma2; //mean value of energy
};
#endif /* NAIVE_OSCILLATOR_H_32504 */
