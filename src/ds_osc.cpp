//  Path Integral Monte Carlo for a single oscillator
//  Copyright (C) 2020 Purrello V.
//  This program is in public domain. It can be modified and relicensed.

#include "ds_osc.h"

#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <random>

DS_osc::DS_osc(double _beta, int _M, long _realizations, bool _VERBOSE) :
    Oscillator(_M, _VERBOSE),
    gen(rd()), beta(_beta), realizations(_realizations), delta_tau(_beta/_M)
{
    if (VERBOSE) {
        std::cerr << "# Direct sampling for oscillator constructed" << std::endl;
    }
}

void DS_osc::run_realizations()
{
    Evir = 0;
    E = 0;
    E2 = 0;
    E_sigma2 = 0;
    E2_sigma2 = 0;
    std::normal_distribution<> gauss{0, std::sqrt(1./(2*tanh(beta/2.)))};
    if (VERBOSE)
        std::cerr << "# Doing " << realizations << " realizations..." << std::endl;
    for (int iter = 0; iter < realizations; ++iter) {
	set_x0 (gauss(gen));
	build_path();
	// https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Welford's_online_algorithm
	const double dEvir = x[0]*x[0] - Evir;
	Evir += dEvir / (iter+1);

	const double new_E = calc_E_exact(delta_tau);
	const double dE = new_E - E;
	E += dE / (iter+1);
	const double dEn = new_E - E;
	E_sigma2 += dE * dEn;

	const double new_E2 = calc_H2_exact(delta_tau);
	const double dE2 = new_E2 - E2;
	E2 += dE2 / (iter+1);
	const double dE2n = new_E2 - E2;
	E2_sigma2 += dE2 * dE2n;
    }
    E_sigma2 /= realizations;
    E2_sigma2 /= realizations;
}

void DS_osc::build_path()
{
    // Algorithm 3.6
    if (VERBOSE)
        std::cerr << "# Building path..." << std::endl;

    const double sinh_dtau = std::sinh(delta_tau);
    const double coth_dtau = std::cosh(delta_tau) / sinh_dtau;
    const double xl = x[0];

    for (int k = 1; k < M; ++k) {
	const double sinh_last = std::sinh((M-k)*delta_tau);
	const double coth_last = std::cosh((M-k)*delta_tau) / sinh_last;
	const double gamma1 = coth_dtau + coth_last;
	const double gamma2 = x[k-1] / sinh_dtau + xl / sinh_last;
	const double mean_xk = gamma2 / gamma1;
	const double sigma = 1. / std::sqrt(gamma1);
	std::normal_distribution<> gauss{mean_xk, sigma};
	x[k] = gauss(gen);
    }
}

double DS_osc::get_Evir()
{
    return Evir;
}

double DS_osc::get_E()
{
    return E;
}

double DS_osc::get_E2()
{
    return E2;
}

double DS_osc::get_E_sigma2()
{
    return E_sigma2;
}

double DS_osc::get_E2_sigma2()
{
    return E2_sigma2;
}

double DS_osc::get_Cv()
{
    return (E2 - E*E) * (beta*beta);
}
