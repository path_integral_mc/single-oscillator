//  Path Integral Monte Carlo for a single oscillator
//  Copyright (C) 2020 Purrello V.
//  This program is in public domain. It can be modified and relicensed.

#include "naive_osc.h"

#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <random>

Naive_osc::Naive_osc(double _beta, int _M, long _MCsteps, double _delta, bool _VERBOSE) :
    Oscillator(_M, _VERBOSE), gen(rd()), rng_real_01(std::uniform_real_distribution<>(0.0,1.0)), 
    rng_int_0M(std::uniform_int_distribution<>(0,_M)),
    beta(_beta), MCsteps(_MCsteps), delta(_delta), delta_tau(_beta/_M)
{
    if (VERBOSE) {
        std::cerr << "# Naive oscillator constructed" << std::endl;
    }
}

void Naive_osc::thermalize() 
{
    int therm_steps = MCsteps / 5, acceptances = 0;
    if (VERBOSE)
        std::cerr << "# Doing " << therm_steps << " thermalization steps ..." << std::endl;
    for (int iter = 0; iter < therm_steps; ++iter)
        for (int j = 0; j < M; ++j)
            if (Metropolis_step_accepted())
                ++acceptances;

    if (VERBOSE)
        std::cerr << "# Percentage of accepted steps = "
            << acceptances / double(M * therm_steps) * 100.0 << std::endl;
}

void Naive_osc::run_steps() 
{
    int acceptances = 0;
    Evir = 0;
    E = 0;
    E2 = 0;
    E_sigma2 = 0;
    E2_sigma2 = 0;
    if (VERBOSE)
        std::cerr << "# Doing " << MCsteps << " production steps ..." << std::endl;
    for (int iter = 0; iter < MCsteps; ++iter) {
        for (int j = 0; j < M; ++j) {
            if (Metropolis_step_accepted())
                ++acceptances;
        }
	const double dEvir = x[0]*x[0] - Evir;
	Evir += dEvir / (iter+1);

	const double new_E = calc_E(delta_tau);
	const double dE = new_E - E;
	E += dE / (iter+1);
	const double dEn = new_E - E;
	E_sigma2 += dE * dEn;

	const double new_E2 = calc_H2(delta_tau);
	const double dE2 = new_E2 - E2;
	E2 += dE2 / (iter+1);
	const double dE2n = new_E2 - E2;
	E2_sigma2 += dE2 * dE2n;
    }
    E_sigma2 /= MCsteps;
    E2_sigma2 /= MCsteps;

    if (VERBOSE)
        std::cerr << "# Percentage of accepted steps = "
            << acceptances / double(M * MCsteps) * 100.0 << std::endl;
}

double Naive_osc::get_Evir()
{
    return Evir;
}

double Naive_osc::get_E()
{
    return E;
}

double Naive_osc::get_E2()
{
    return E2;
}

double Naive_osc::get_E_sigma2()
{
    return E_sigma2;
}

double Naive_osc::get_E2_sigma2()
{
    return E2_sigma2;
}

double Naive_osc::get_Cv()
{
    return (E2 - E*E) * (beta*beta);
}

bool Naive_osc::Metropolis_step_accepted() 
{
    int k = rng_int_0M(gen);
// indexes of neighbors and PBC 
    int k_minus = k - 1, k_plus = k + 1;
    if (k_minus == -1) k_minus = M - 1;
    if (k_plus == M) k_plus = 0;
// choose a random trial displacement
    double x_trial = x[k] + (2*rng_real_01(gen) - 1) * delta;
// compute change in energy
// Eq. 3.13 but with delta_tau instead of beta
    double pi_a = rho_free(x[k_minus], x[k], delta_tau)
                * rho_free(x[k], x[k_plus], delta_tau)
                * exp(-0.5 * delta_tau * x[k] * x[k]);
    double pi_b = rho_free(x[k_minus], x_trial, delta_tau)
                * rho_free(x_trial, x[k_plus], delta_tau)
                * exp(-0.5 * delta_tau * x_trial * x_trial);
    if (pi_b/pi_a > rng_real_01(gen)) {
        x[k] = x_trial;
        return true;
    }
    return false;
}
