//  Path Integral Monte Carlo for a single oscillator
//  Copyright (C) 2020 Purrello V.
//  This program is in public domain. It can be modified and relicensed.

#ifndef DIRECT_SAMPLING_OSCILLATOR_H_14840
#define DIRECT_SAMPLING_OSCILLATOR_H_14840

#include "oscillator.h"
#include <vector>
#include <random>

class DS_osc: public Oscillator
{
    public:
        DS_osc (double _beta, int _M=8, long _realizations=1e6, bool _VERBOSE=true);
        void run_realizations();
        void build_path();
        double get_Evir();
        double get_E();
        double get_E_sigma2();
        double get_E2();
        double get_E2_sigma2();
        double get_Cv();

    protected:
        std::random_device rd;  //Will be used to obtain a seed for the random number engine
        std::mt19937 gen; //Standard mersenne_twister_engine seeded with rd()
        const double beta;
        const long realizations; //number of realizations
        const double delta_tau; //imaginary time step
        double Evir, E, E_sigma2, E2, E2_sigma2; //mean value of energy
};
#endif /* DIRECT_SAMPLING_OSCILLATOR_H_14840 */
