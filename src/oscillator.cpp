//  Path Integral Monte Carlo for a single oscillator
//  Copyright (C) 2020 Purrello V.
//  This program is in public domain. It can be modified and relicensed.

#include "oscillator.h"

#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>

Oscillator::Oscillator(int _M, bool _VERBOSE) :
    M(_M), VERBOSE(_VERBOSE), x(_M)
{
    if (VERBOSE) {
        std::cerr << "# Oscillator constructed" << std::endl;
    }
}

void Oscillator::print_x()
{
    for (auto xj : x)
        std::cout << xj << '\n';
}

void Oscillator::set_x0(double x0)
{
    x.at(0) = x0;
}

double Oscillator::calc_E(double beta)
{
//    return x[0]*x[0]/2. //Ec
//	   - partial2_rho_free(x[0], x[1], beta)/2.; //Ep
    double aux = 0;
    for (int i = 0; i < M-1; ++i)
	aux += (x[i]*x[i]/2. - partial2_rho_free(x[i], x[i+1], beta)/2.);
    return aux/(M-1);
}

double Oscillator::calc_E_exact(double beta)
{
    const double tanh = std::tanh(beta/2.);
    const double coth = 1./tanh;
    double E = 0;
    for (int i = 0; i < M-1; ++i) {
	const double dE = x[i]*x[i]/2. + ((coth + tanh) - pow((x[i]+x[i+1])*tanh + (x[i]-x[i+1])*coth,2)/2.) /4.
	    - E;
	E += dE / (i+1);
    }
    return E;
}

double Oscillator::calc_H2(double beta)
{
//    return (x[0]*x[0]/2. - partial2_rho_free(x[0], x[1], beta)/2.)
//	   * (x[1]*x[1]/2. - partial2_rho_free(x[1], x[2], beta)/2.);
    double aux = 0;
    for (int i = 0; i < M-2; ++i)
	aux += (x[i]*x[i]/2. - partial2_rho_free(x[i], x[i+1], beta)/2.)
	   * (x[i+1]*x[i+1]/2. - partial2_rho_free(x[i+1], x[i+2], beta)/2.);
    return aux/(M-2);
}

double Oscillator::calc_H2_exact(double beta)
{
    const double tanh = std::tanh(beta/2.);
    const double coth = 1./tanh;
    double H2 = 0;
    for (int i = 0; i < M-2; ++i) {
	const double dH2 = (x[i]*x[i]/2. + ((coth + tanh) - pow((x[i]+x[i+1])*tanh + (x[i]-x[i+1])*coth,2)/2.) /4.)
	    * (x[i+1]*x[i+1]/2. + ((coth + tanh) - pow((x[i+1]+x[i+2])*tanh + (x[i+1]-x[i+2])*coth,2)/2.) /4.)
	    - H2;
	H2 += dH2 / (i+1);
    }
    return H2;
}

double Oscillator::rho_free(double x1, double x2, double beta)
{
    return sqrt(1./(2*M_PI*beta)) * exp(-(x1-x2)*(x1-x2)/(2*beta));
}

double Oscillator::partial2_rho_free(double x1, double x2, double beta)
{
    return pow((x1-x2)/beta + beta*x1/2., 2) - 1./beta - beta/2.;
}

double Oscillator::rho(double x1, double x2, double beta)
{
    return exp(-beta*V(x1)/2.) * rho_free(x1, x2, beta) * exp(-beta*V(x2)/2.);
}

double Oscillator::V(double x)
{
    return x*x/2.;
}

double Oscillator::Z(double beta)
{
    double acum = 0;
    for (int i = 0; i < M-1; ++i) {
        double x1 = x[i];
        double x2 = x[(i+1==M ? 0 : i+1)];
        acum += rho(x1, x2, beta);
    }
    return acum;
}

double Oscillator::rho_ho(double x1, double x2, double beta)
{
    double c_dtau = sqrt(1. / (2*M_PI*sinh(beta))); //as shown on Eq. (3.37)
    double g_dtau = tanh(beta/2) / 2;
    double f_dtau = 1. / (2*tanh(beta/2));
//Eq. (3.33)
    return c_dtau * exp(-g_dtau * (x1-x2)*(x1-x2) / 2 - f_dtau * (x1+x2)*(x1+x2) / 2);
}
