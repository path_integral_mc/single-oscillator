# Path sampling Monte Carlo methods for a single oscillator

These algorithms are taken from the book *Statistical Mechanics: Algorithms and Computations*, by Werner Krauth.

The naive path sampling algorithm, Algorithm 3.4 in p. 151, is implemented in [naive\_osc.h](src/naive\_osc.h), with an example use showcased in [single\_naive\_osc.cpp](examples/single\_naive\_osc.cpp).

The direct path sampling algorithm, Algorithm 3.6 in p. 155, is implemented in [ds\_osc.h](src/ds\_osc.h), with an example use showcased in [single\_direct\_osc.cpp](examples/single\_direct\_osc.cpp).

## Development Process

Simply build and choose an example to run.

```
# Clone the repo:
git clone https://gitlab.com/path_integral_mc/single-oscillator.git
cd single-oscillator

# Build:
make

# Run:
cd build
./single_naive_osc
```

## Options for example single\_naive\_osc

| Option		| Description				|
|-----------------------|---------------------------------------|
| -b \<value\>		|beta (Default: 4)			|
| -m \<value\>		|M (Default: 8)				|
| -s \<value\>		|Number of MC steps (Default: 1e6)	|
| -d \<value\>		|delta for MC (Default: 1)		|
| -v 			|Verbose mode				|
| -h 			|Print help and exit			|

## Options for example single\_direct\_osc

| Option		| Description				|
|-----------------------|---------------------------------------|
| -b \<value\>		|beta (Default: 4)			|
| -m \<value\>		|M (Default: 8)				|
| -r \<value\>		|Number of realizations (Default: 100)	|
| -v 			|Verbose mode				|
| -h 			|Print help and exit			|

## License

This program is in public domain. It can be modified and relicensed.
