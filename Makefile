#  Path Integral Monte Carlo for a single oscillator
#  Copyright (C) 2020 Purrello V.
#  This program is in public domain. It can be modified and relicensed.

SHELL			= /bin/sh

# Paths & Includes
OUTDIR			= $(CURDIR)/build
SRCDIR			= $(CURDIR)/src
EXAMPLESDIR		= $(CURDIR)/examples
VPATH			= $(SRCDIR):$(EXAMPLESDIR)
INCLUDES		= -I$(SRCDIR)

LDFLAGS			=
LDLIBS			=
WARN			= -Wall -Wextra
OPTIMIZATION		= -O3 -ffast-math
CXX			= g++
CPPFLAGS		= $(INCLUDES)
CXXFLAGS		= $(OPTIMIZATION) $(WARN)
DEPFLAGS		= -MT $@ -MMD -MP -MF $(OUTDIR)/$*.d
COMPILE.cc		= $(CXX) $(DEPFLAGS) $(CXXFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c

SOURCES_SRC		= $(shell cd $(SRCDIR) && ls *.cpp)
EXAMPLES_SRC		= $(shell cd $(EXAMPLESDIR) && ls *.cpp)
EXAMPLES		= $(EXAMPLES_SRC:.cpp=)
DEPS			:= $(SOURCES_SRC:%.cpp=$(OUTDIR)/%.d) $(EXAMPLES_SRC:%.cpp=$(OUTDIR)/%.d)
# DEPS is explained in https://make.mad-scientist.net/papers/advanced-auto-dependency-generation/

# Rules
.PHONY: all clean

all: $(patsubst %, $(OUTDIR)/%, $(EXAMPLES))

$(DEPS):
include $(wildcard $(DEPS))

$(OUTDIR)/%.o : %.cpp $(OUTDIR)/%.d | $(OUTDIR)
	$(COMPILE.cc) -o $@ $<

$(OUTDIR): ; @mkdir -p $@

clean:
	rm -fv $(OUTDIR)/*.d $(OUTDIR)/*.o $(patsubst %, $(OUTDIR)/%, $(EXAMPLES))
	rmdir $(OUTDIR)

$(OUTDIR)/single_naive_osc: $(patsubst %, $(OUTDIR)/%, single_naive_osc.o oscillator.o naive_osc.o)
	$(LINK.cc) -o $@ $^ $(LDLIBS)

$(OUTDIR)/single_direct_osc: $(patsubst %, $(OUTDIR)/%, single_direct_osc.o oscillator.o ds_osc.o)
	$(LINK.cc) -o $@ $^ $(LDLIBS)
