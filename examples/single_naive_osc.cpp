//  Path Integral Monte Carlo for a single oscillator
//  Copyright (C) 2020 Purrello V.
//  This program is in public domain. It can be modified and relicensed.

#include "naive_osc.h"
#include <iostream>
#include <unistd.h>

void usage_and_exit (char* exe) {
    std::cerr << "Usage: " << exe << " [options]\n" << std::endl;
    std::cerr << "Options:\n" << std::endl;
    std::cerr << "  -b <value>\tbeta (Default: 4)" << std::endl;
    std::cerr << "  -m <value>\tM (Default: 8)" << std::endl;
    std::cerr << "  -s <value>\tNumber of MC steps (Default: 1e6)" << std::endl;
    std::cerr << "  -d <value>\tdelta for MC (Default: 1)" << std::endl;
    std::cerr << "  -v \t\tVerbose mode" << std::endl;
    std::cerr << "  -h \t\tPrint this help and exit" << std::endl;
    exit (1);
}

int main(int argc, char **argv)
{
// Parsing Arguments
    int c;
    double beta = 4;
    int M = 8;
    long MCsteps = 1e6;
    double delta = 1;
    bool verbose = false;

    while ((c = getopt (argc, argv, "b:m:s:d:vh")) != -1)
        switch (c)
        {
            case 'b':
                if (! atof (optarg))
                    usage_and_exit(argv[0]);
                beta = std::atof (optarg);
                break;
            case 'm':
                if (! atof (optarg))
                    usage_and_exit(argv[0]);
                M = std::atoi (optarg);
                break;
            case 's':
                if (! atof (optarg))
                    usage_and_exit(argv[0]);
                MCsteps = std::atol (optarg);
                break;
            case 'd':
                if (! atof (optarg))
                    usage_and_exit(argv[0]);
                delta = std::atof (optarg);
                break;
            case 'v':
                verbose = true;
                break;
            case 'h':
                usage_and_exit(argv[0]);
                break;
            case '?':
                usage_and_exit(argv[0]);
                break; // avoids warning on fallthrough since C++17
            default:
                usage_and_exit(argv[0]);
        }

    Naive_osc osc(beta, M, MCsteps, delta, verbose);
    osc.thermalize();
    osc.run_steps();
    const double Evir = osc.get_Evir();
    const double E = osc.get_E();
    const double E_sigma2 = osc.get_E_sigma2();
    const double E_err = std::sqrt(E_sigma2/MCsteps);
    const double E2 = osc.get_E2();
    const double E2_sigma2 = osc.get_E2_sigma2();
    const double E2_err = std::sqrt(E2_sigma2/MCsteps);
    const double Cv = osc.get_Cv();
    const double Cv_err = std::sqrt((E2_sigma2 + 4*E*E*E_sigma2) / MCsteps);
    std::cout << beta << " " << E << " " << E_err << " " << Cv << " " << Cv_err << " " << Evir << " " << E2 << " " << E2_err << std::endl;
    return 0;
}
